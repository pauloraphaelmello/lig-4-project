let buttonHome = document.getElementById('buttons_home');
let buttonReset = document.getElementById('buttons_reset');

buttonReset.addEventListener('click', function () {
    return window.location.href = window.location.href;
});

buttonHome.addEventListener('click', function () {
    return window.location.href = `../index.html`;
});

class Player {
    constructor(code, name, color) {
        this.code = code;
        this.name = name;
        this.color = color;
    }
    changeColor (newColor){
        this.color = newColor;
    }
    changeName (newName){
        this.name = newName;
    }
}
const player1 = new Player(1, 'Player1', `#F00`);
const player2 = new Player(2, 'Player2', `#000`);

// POP UP PLAYER 
const playerOne = document.getElementById('playerOne')
const playerTwo = document.getElementById('playerTwo')

const palette = document.querySelector('.palette-color')
const popUpPlayer = document.querySelector('.popup-player')

let clickedPlayer 

playerOne.addEventListener('click', () => {
    clickedPlayer = player1
})
playerTwo.addEventListener('click', () => {
    clickedPlayer = player2
})

palette.addEventListener('click', event => {
    let idColor = event.target.dataset.color
    if(idColor !== undefined && clickedPlayer !== undefined){
        clickedPlayer.changeColor(idColor)

        if(clickedPlayer === player1){
            playerOne.style.background = idColor
        } else {
            playerTwo.style.background = idColor
        }
    }
})

const player01 = document.getElementById('player1')
const player02 = document.getElementById('player2')

const start = document.getElementById('start')
start.addEventListener('click', () => {
    if(player1.color === player2.color){
        return alert("Mudar cor de um dos jogadores!")
    }
    
    player1.changeName(playerOne.value)
    player2.changeName(playerTwo.value)

    player01.value = player1.name
    player01.style.background = player1.color
    player02.value = player2.name
    player02.style.background = player2.color

    popUpPlayer.style.display = 'none'

    let game = document.querySelector('.game')
    game.classList.add('initial')

    showTurn()
})

//Lógica do jogo
let game_active = false;
let active_player = 0;
let gameboard = [];

function beginGame() {
    if (game_active === false){
        game_active = true;
    }
    for (let row = 0; row <= 5; row++) {
        gameboard[row] = [];
        for (let col = 0; col <= 6; col++) {
            gameboard[row][col] = 0;
        }
    }
    muchPieces = 0;
    drawBoard();
    active_player = 1;
}

function drawBoard() {
    checkWin();
    for (let col = 0; col <= 6; col++) {
        for (let row = 0; row <= 5; row++) {
            let cell = document.getElementById(`square_${row}_${col}`)
            cell.innerHTML = "<span class='piece player" + gameboard[row][col] + "'> </span>";
            let childCell = cell.firstElementChild
            if(childCell.classList[1].includes(1)){
                childCell.style.background = player1.color
            }
            if(childCell.classList[1].includes(2)){
                childCell.style.background = player2.color
            }
        }
    }
}

let maxPieces = 42;
let muchPieces = 0;
const drawGame = () =>{
    muchPieces++;
}

function checkWin() {
    for (let i = 1; i <= 2; i++) {
        for (let col = 0; col <= 3; col++) {
            for (let row = 0; row <= 5; row++) {
                if (gameboard[row][col] == i) {
                    if ((gameboard[row][col + 1] == i) && (gameboard[row][col + 2] == i) && (gameboard[row][col + 3] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }

    for (let i = 1; i <= 2; i++) {
        for (let col = 0; col <= 6; col++) {
            for (let row = 0; row <= 2; row++) {
                if (gameboard[row][col] == i) {
                    if ((gameboard[row + 1][col] == i) && (gameboard[row + 2][col] == i) && (gameboard[row + 3][col] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }

    for (let i = 1; i <= 2; i++) {
        for (let col = 0; col <= 3; col++) {
            for (let row = 0; row <= 2; row++) {
                if (gameboard[row][col] == i) {
                    if ((gameboard[row + 1][col + 1] == i) && (gameboard[row + 2][col + 2] == i) && (gameboard[row + 3][col + 3] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }

    for (let i = 1; i <= 2; i++) {
        for (let col = 0; col <= 3; col++) {
            for (let row = 3; row <= 5; row++) {
                if (gameboard[row][col] == i) {
                    if ((gameboard[row - 1][col + 1] == i) && (gameboard[row - 2][col + 2] == i) && (gameboard[row - 3][col + 3] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }
    if(maxPieces === muchPieces){
        endGame(3)
        return true;
    }else{
        drawGame();
    }
}

let closePopupGameResult = document.getElementById('closePopupGameResult');
let game_result = document.getElementById('game_result');
closePopupGameResult.addEventListener('click', () => {
    game_result.classList.add('hidden');
    beginGame();
})

const drawClose = document.getElementById(`closePopupDrawGameResult`)
const drawGameEl = document.getElementById(`drawGame_result`)
drawClose.addEventListener(`click`, ()=>{
    drawGameEl.classList.add(`hidden`);
    beginGame();
})

const soundEl = document.getElementById(`sound`);
let name_winning_player = document.getElementById('winning_player');
let countVictoryPlayer1 = 0;
let countVictoryPlayer2 = 0;

function endGame(winningPlayer) {
    if (winningPlayer === 1) {
        name_winning_player.innerText = player1.name;
        countVictoryPlayer1++;
        document.getElementById('player1Points').innerText = countVictoryPlayer1;
    }
    if (winningPlayer === 2) {
            name_winning_player.innerText = player2.name;
            countVictoryPlayer2++;
            document.getElementById('player2Points').innerText = countVictoryPlayer2;
    }
    
    if(winningPlayer === 3){
        drawGameEl.classList.remove(`hidden`);
        game_active = false;
        return;
    }
    console.log(countVictoryPlayer2)

    soundEl.play();
    setTimeout(stop(), 3000);
    game_active = false;
    game_result.classList.remove('hidden'); //popup vitoria aparece

    //zerar as bordas e recomeçar no play 1
    document.getElementById('player1').style.border = "3px solid white";
    document.getElementById('player2').style.border = "none";
}

function showTurn() {
    if (game_active) {
        if (active_player === 1) {
            document.getElementById('player1').style.border = "3px solid white"
            document.getElementById('player2').style.border = "none";
        } else {
            if (active_player === 2) {
                document.getElementById('player2').style.border = "3px solid white"
                document.getElementById('player1').style.border = "none";
            }
        }
    }
}

function drop(col) {
    for (let row = 5; row >= 0; row--) {
        if (gameboard[row][col] == 0) {
            gameboard[row][col] = active_player;
            drawBoard();
            if (active_player === 1) {
                active_player = 2;
            } else {
                active_player = 1;
            }
            showTurn();
            return true;
        }
    }
}

function teste(div, i) {
    let teste = document.getElementById(div);
    teste.addEventListener('click', () => {
        drop(i);
    })
}

for (let i = 0; i <= 6; i++) {
    for (let j = 0; j <= 5; j++) {
        teste(`square_${j}_${i}`, i);
    }
}
